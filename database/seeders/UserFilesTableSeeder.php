<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Webpatser\Uuid\Uuid;

class UserFilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create sample files data
        $faker = Faker::create();
        $files = array("test.jpg","test.png","test.pdf","test.doc");


        foreach (range(1,10) as $index) {
            $file = $files[array_rand($files, 1)];
            $extension = explode('.', $file);
            DB::table('user_files')->insert([
                'uuid' => Uuid::generate(),
                'file_name' => $file,
                'type' => $extension[1],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
