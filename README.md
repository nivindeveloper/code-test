# README #

This README would normally document whatever steps are necessary to get your application up and running.

## About The Project

White Rabbit Group Code Test with following features.

Simple Directory listing
1. Listing files in Directory (Folder) with pagination, search and delete.
2. Search should support regular expression (eg: *.pdf).
3. Upload file feature with validation : size: 2 MB, Format:
txt,doc,docx,pdf,png,jpeg,jpg,gif
4. History of upload and deleted files with pagination
5. Neat & simple User interface

### Built With

This section should list any major frameworks/libraries used to bootstrap your project. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.

* [Laravel 8](https://laravel.com)
* [React.js](https://reactjs.org)
* [Redux](https://react-redux.js.org)
* [React Bootstrap](https://react-bootstrap.github.io)

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo


   git clone https://nivindeveloper@bitbucket.org/nivindeveloper/code-test.git

2. Install Composer

   composer install

3. Install NPM packages

   npm install

4. Enter your database credentials in `.env` 


5. Enter your Base API URL in `resources/js/utils/request.js`
 
   const baseURL = 'ENTER YOUR API';

6. Enter your database credentials in `.env` 


7. Migrate database

    php artisan migrate 

8. Run db:seed for creating sample files data

    php artisan db:seed 

8. Run project

    php artisan serve 

## Contact

Nivin S -  nivin1392@gmail.com

Project Link: git clone https://nivindeveloper@bitbucket.org/nivindeveloper/code-test.git
