import axios from "axios";

const baseURL = "/api/";

//For get method
function request(url) {
    const headers = {
        Accept: "application/json"
    };
    const requestUrl = baseURL + url;

    return axios({
        method: "GET",
        url: requestUrl,
        headers
    });
}

//For post method with parameter
function postRequest(url, params) {
    const headers = {
        Accept: "application/json"
    };
    const requestUrl = baseURL + url;

    return axios({
        method: "POST",
        url: requestUrl,
        data: params,
        headers
    });
}

//For delete method with parameter
function deleteRequest(url, params) {
    const headers = {
        Accept: "application/json"
    };
    const requestUrl = baseURL + url;

    return axios({
        method: "DELETE",
        url: requestUrl,
        data: params,
        headers
    });
}

export { request, postRequest, deleteRequest };
