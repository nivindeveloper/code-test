import React from "react";
import { Image } from 'react-bootstrap';

const Loader = () => (
    <>
        <Image src="/home/loader.gif"  width="100" height="100"/>
    </>
);

export default Loader;
