import React from "react";
import { Form, Button, Alert } from 'react-bootstrap';

import Joi from 'joi-browser';
import { toast } from "react-toastify";

import axios, { post } from 'axios';

class Add extends React.Component{

    constructor(){
        super();
        this.state = {
            upload_file:'',
            errorMsg: "",
            successMsg: "",
        }

        //this.handleInputChange = this.handleInputChange.bind(this);
    }

    schema = Joi.object() // Form validation schema object
        .keys({
            upload_file: Joi.any().required().label("Please select the file")
        }).unknown(true);

    //Update input change
    handleInputChange = (event) => {
        const error = { ...this.state.error };
        const errorMassage = this.validationProperty(event);
        if (errorMassage) {
            error[event.target.name] = errorMassage;
        } else {
            delete error[event.target.name];
        }

        this.setState({
            upload_file: event.target.files[0],
            errorMsg: "",
            successMsg: "",
            error
        });
    };

    validationProperty = (event) => {
        const Obj = { [event.target.name]: event.target.value };
        if (this.schema[event.target.name] != undefined) {
            const schema = { [event.target.name]: this.schema[event.target.name] };
            const { error } = Joi.validate(Obj, schema);
            return error ? error.details[0].message : null;
        } else {
            return null;
        }
    };

    validate = () => {
        const options = { abortEarly: false };

        const form = {
            upload_file: this.state.upload_file
        };
        const { error } = Joi.validate(form, this.schema, options);

        if (!error) return null;

        const errors = {};
        for (const item of error.details) errors[item.path[0]] = item.message;
        return errors;
    };

    submitHandler = (event) => {
        event.preventDefault();
        const error = this.validate();

        this.setState({error: error || {}});

        if (!error) {        //Assign submitted form data
            const data = new FormData();
            data.append('file', this.state.upload_file)
            let url = "/api/add";

            axios.post(url, data, { // receive two parameter endpoint url ,form data 
            })
            .then(res => {
                //If the response is success show the success message and redirect to home page
                if(res.data.success){
                    this.setState({successMsg:res.data.message, upload_file:''});
                    setTimeout(() => {
                        window.location = "/";
                    }, 1000);
                }else{
                    this.setState({errorMsg:res.data.message});
                }
            })
        }else{
            toast.error(error);
        }
    }

    render(){
        return(
            <div>
                <div className="row mt-5">
                    <div className="col-md-6 offset-md-3">
                        {
                            this.state.successMsg != "" ? (
                                <Alert variant="success">{this.state.successMsg}</Alert>
                            ) : null
                        }
                        <h3>File Upload</h3>
                        <Form onSubmit={this.submitHandler}>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label>Select File :</label>
                                    <input type="file" className="form-control" name="upload_file" onChange={this.handleInputChange} />
                                    {
                                        this.state.errorMsg != "" ? (
                                            <p className="text-danger">{this.state.errorMsg}</p>
                                        ) : null
                                    }
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-md-6">
                                    <Button type="submit">Submit</Button>
                                </div>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Add;