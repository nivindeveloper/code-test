import React from "react";
import {connect} from "react-redux";
import { Button, Image } from 'react-bootstrap';
import Moment from 'moment';

import { deleteFile } from '../../actions/files';

//Set the uploaded file path
const path = "/files/";
const default_path = "/home/";

const fileTypes = ["txt", "doc", "docx", "pdf", "gif"];

const Items = ({ file, deleteFile, tab }) => {
    return (
        <>
        <tr>
            <td>
                {
                    fileTypes.indexOf(file.type) > -1 ? (
                        <a href={path+file.file_name}>
                            <Image title={file.file_name} alt={file.file_name} src={default_path+file.type+".png"} thumbnail width="100" height="100"/>
                            <p>{file.file_name}</p>
                        </a>
                    ) : (
                        <>
                        <Image title={file.file_name} alt={file.file_name} src={path+file.file_name} thumbnail width="100" height="100"/>
                            <p>{file.file_name}</p>
                        </>
                    )
                }
            </td>
            <td>{file.type}</td>
            <td>{Moment(file.created_at).format('DD-MM-YYYY')}</td>
            <td>{Moment(file.deleted_at).format('DD-MM-YYYY')}</td>
            {tab == 0 ? (
                <td>
                    <Button onClick={()=>deleteFile(file.uuid)} variant="danger">Delete</Button>
                </td>
            ) : null
            }
        </tr>
        </>
    )
};

export default connect(null,{deleteFile})(Items);