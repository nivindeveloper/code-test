import React, { Component } from "react";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";

import { Button, Card, Table, Nav, Form, FormControl } from 'react-bootstrap';

import Loader from "../common/loader";
import Items from "./Items";

import "bootstrap/dist/css/bootstrap.min.css";

import { getFiles, deleteFile } from "../../actions/files";

class List extends Component  {

    constructor() {
        super();
        this.state = {
            activeKey : 0,
            searchText: "",
            pageNumber: 1
        }
    }

    componentDidMount() {
        this.props.getFiles({ "tab":0, "page":this.state.pageNumber });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.files != undefined && this.props.files.current_page > 1 && this.props.files.data.length == 0) {
            this.props.getFiles({ 
                "tab":this.state.activeKey, 
                "searchText":this.state.searchText, 
                "page":this.props.files.current_page-1 
            });
        }
    }

    removeFile(id) {
        this.props.deleteFile(id);
    }

    handleSelect = (eventKey) => {
        this.setState({activeKey:eventKey});
        this.props.getFiles({ "tab":eventKey, "page":this.state.pageNumber });
    };

    handleInputChange(event) {
        this.setState({
            searchText: event.target.value,
            pageNumber: 1
        }, () => {
            this.props.getFiles({ "tab":this.state.activeKey, "searchText":this.state.searchText, "page":1 });
        });
    }

    handlePageChange(pageNumber) {
        const { current_page, per_page, total } = this.props.files;

        this.props.getFiles({ "tab":this.state.activeKey, "searchText":this.state.searchText, "page":pageNumber });
    }

    render() {
        const { current_page, per_page, total } = this.props.files != undefined ? this.props.files : [];
        const { activeKey } = this.state;

        return (
            <>
                <Nav variant="pills" activeKey={this.state.activeKey} onSelect={this.handleSelect} className="mt-5 mb-1">
                    <Nav.Item>
                        <Nav.Link eventKey="0">
                            Current
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link eventKey="1" title="Item">
                            History
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="form-inline ">
                        <FormControl type="text" placeholder="Search with file name or type" className="ml-sm-2 mr-sm-2" value={this.state.searchText} onChange={e => this.handleInputChange(e)}/>
                    </Nav.Item>
                </Nav>
                {this.props.loading && <Loader/>}
                {
                    (!this.props.loading && this.props.files != undefined && this.props.files.hasOwnProperty("data") && this.props.files.data.length > 0) ? (
                        <>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>File Name</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                    {
                                        activeKey == 0 ? (
                                            <th>Action</th>
                                        ) : (
                                            <th>Deleted</th>
                                        )
                                    }
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.files.data.map(file=>
                                        <Items key={file.id} file={file} tab={activeKey}/>
                                    )}
                                </tbody>
                            </Table>
                            <Pagination
                                activePage={current_page}
                                itemsCountPerPage={per_page}
                                totalItemsCount={total}
                                onChange={this.handlePageChange.bind(this)}
                                itemClass="page-item"
                                linkClass="page-link"
                                firstPageText="First"
                                lastPageText="Last"
                            />
                        </>
                    ) : (
                        <Card>
                            <Card.Body>No records found.</Card.Body>
                        </Card>
                    )
                }
            </>
        )
    }
}

const mapStateToProps = state =>  ({
    //Set file reducer data
    files: state.fileReducer.data,
    loading: state.fileReducer.loading
})

const mapDispatchToProps = (dispatch) => {
    return {
        getFiles: (data) => {
            dispatch(getFiles(data));
        },
        deleteFile: (id) => {
            dispatch(deleteFile(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);