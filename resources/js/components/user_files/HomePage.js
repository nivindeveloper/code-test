import React from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import { withRouter } from "react-router-dom";

import List from "./List";

class HomePage extends React.Component{
    render(){
        return (
            <Container>
                <Row>
                    <Col lg={12} md={12}>
                        <List/>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default withRouter(HomePage);
