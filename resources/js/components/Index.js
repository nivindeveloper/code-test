import React from 'react';
import ReactDOM from 'react-dom';

import AppRouter from './AppRouter';
import { Provider } from 'react-redux'
import store from '../store';

export default class Index extends React.Component{
    render() {
        return (
            <Provider store={store}>
                <AppRouter/>
            </Provider>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}
