import React from 'react';
import { Navbar, Container, Row, Col } from 'react-bootstrap';

const Footer = () => (
    <footer className={'footer fixed-bottom'}>
        <Navbar>
            <Container className="justify-content-md-center">
                <Row>
                    <Col lg={12}>
                        <div className={"text-center copyright-height"}>
                            Copyright &copy; {(new Date().getFullYear())}
                        </div>
                    </Col>
                </Row>
            </Container>
        </Navbar>
    </footer>
);

export default Footer;