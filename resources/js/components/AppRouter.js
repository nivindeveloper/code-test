import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { ToastContainer } from "react-toastify";

import Header from './Header';
import Footer from './Footer';
import List from './user_files/List';
import Add from './user_files/Add';

import 'react-toastify/dist/ReactToastify.css';
export default class AppRouter extends React.Component{
    render() {
        return (
            <BrowserRouter>
                <Container fluid>
                    <Header/>
                    <ToastContainer />
                    <Switch>
                        <Route path="/" component={List} exact={true} />
                        <Route path="/add" component={Add} exact={true}/>
                    </Switch>
                    <Footer/>
                </Container>
            </BrowserRouter>
        );
    }
}
