import { LOAD_FILES, GET_FILES, DELETE_FILE } from "../actions/types";

const initialState = {
    data : [],
    loading: true,
    error: {}
}

export default function(state=initialState, action) {
    const {type, payload} = action;

    switch(type){
        case LOAD_FILES:
            return{
                loading : true
            }
        case GET_FILES:
            return{
                ...state,
                data : payload,
                loading : false
            }
        case DELETE_FILE:
            return{
                ...state,
                data: {
                    ...state.data,
                    data: state.data.data.filter(item => item.uuid !== action.payload),
                },
                loading : false
            }
        default:
            return state;
    }
}