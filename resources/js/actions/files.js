import { LOAD_FILES, GET_FILES, DELETE_FILE, POST_ERROR } from "./types";
import { toast } from "react-toastify";

import { request, postRequest, deleteRequest } from "../utils/request";

//Add file action
export async function addFile(formData)
{
    try {
        return postRequest("add", formData)
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Get all file list action
export const getFiles = (formData) => async dispatch => 
{
    try {
        dispatch({
            type: LOAD_FILES
        });
        const res = await postRequest("list?page="+formData.page, formData);

        dispatch({
            type: GET_FILES,
            payload: res.data
        });
    } catch (error) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: error.response.statusText,
                status: error.response.status
            }
        });
    }
}

//Delete file action
export const deleteFile = (uuid) => async dispatch => 
{
    try {
        const res = await deleteRequest("file/"+uuid);

        dispatch({
            type: DELETE_FILE,
            payload: res.data.uuid
        })
    } catch (err) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: err.response.statusText,
                status: err.response.status
            }
        })
    }
}