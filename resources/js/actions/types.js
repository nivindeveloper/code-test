export const LOAD_FILES = "LOAD_FILES";
export const GET_FILES = "GET_FILES";
export const DELETE_FILE = "DELETE_FILE";
export const FILES_ERROR = "FILE_ERROR";
export const POST_ERROR = "POST_ERROR";
