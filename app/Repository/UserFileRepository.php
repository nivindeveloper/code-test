<?php
namespace App\Repository;

use App\Services\CommonService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Models\UserFile;

class UserFileRepository
{
    /**
     * File details save process.
     * Step 1: Check the file id for edit the record
     * Step 2: If record id not found generate new unique id
     * Step 3: Set the file name and extension from temp file
     * Step 4: Upload the file into given directory
     * Step 5: Save the new record with file name
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function saveProcess(Request $request)
    {
        $response = ['success' => false, 'message' => __('Invalid request')];
        try {
            if($request->id){
                $userFile = UserFile::find($request->id);
            }else{
                $userFile = new UserFile();
                $userFile->uuid = Uuid::generate();
            }

            $folderPath = "files/";
            $file_tmp = $_FILES['file']['tmp_name'];
            //For getting file extension
            $file_ext = explode('.',$_FILES['file']['name']);

            $fileName = time().'.'.$file_ext[1];
            $file =  $folderPath . $fileName;
            move_uploaded_file($file_tmp, $file);

            $userFile->file_name = $fileName;
            $userFile->type = $file_ext[1];

            if ($userFile->save()){
                $response = [
                    'success' => true,
                    'message' => __("File uploaded successfully")
                ];
            }else{
                $response = [
                    'success' => false,
                    'message' => __('Failed to upload')
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return $response;
        }

        return $response;
    }

    /**
     * Files details list process.
     * Step 1: Set the selectable field into an array
     * Step 2: Add condition if the search text exist
     * Step 3: Check the tab values for showing active and deleted records
     * Step 4: Return the records with paginate data
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function listProcess($request)
    {
        $select = ["id", "uuid", "file_name", "type", "created_at"];

        //Check regular expression with Search text(eg *.pdf, *,txt)
        $search = preg_replace('/^.*\.([^.]+)$/D', '$1', request('searchText'));

        $files = UserFile::select($select)->where(
            function($q) use ($request, $search){
                if($request->has('searchText') && !empty($search)){
                    $q->orWhere('file_name','LIKE','%' . $search . '%')->orWhere('type','LIKE','%' . $search . '%');
                }
        });
        if($request['tab']){
            $files = $files->onlyTrashed();
        }
        $files = $files->orderBy('id', 'desc')->paginate(10);

        return $files;
    }
}
