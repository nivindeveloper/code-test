<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * formating errors array 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    function errorFormat($objects){
        $return_arr = [];
        $errors = [];
        $errors =  json_decode($objects);
        $errors = (array)$errors;
        if(!empty($errors)){
            foreach ($errors as $field =>$errortype){
                foreach ($errortype as $errorkey =>$error){
                    $return_arr[] = $error;
                }
            }
        }
        return !empty($return_arr[0]) ? $return_arr[0] : 'No errors';
    }
}
