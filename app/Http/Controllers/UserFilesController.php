<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Validator;

use App\Repository\UserFileRepository;

use App\Models\UserFile;

class UserFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //For get all files list using User File Repository
        $userFiles = app(UserFileRepository::class)->listProcess($request);

        return response()->json($userFiles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnArr = [];
        $validator = Validator::make($request->all(),[
            'file' => 'required|mimes:png,txt,doc,docx,pdf,jpeg,jpg,gif|max:2048',
        ],
        [
            'file.max' => 'The file size should be less than 2MB.',
        ]);
        if(!$validator->fails()){
            $returnArr = app(UserFileRepository::class)->saveProcess($request);
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = $this->errorFormat($validator->messages());
        }
        return response()->json($returnArr);
    }

    /**
     * Remove the specified file record from database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(UserFile $userFile)
    {
        if($userFile->delete()) {
            $returnArr['success'] = 1;
            $returnArr['message'] = 'File successfully deleted';
            $returnArr['uuid'] = $userFile->uuid;
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = 'Something went wrong';
        }
        return response()->json($returnArr);
    }
}
